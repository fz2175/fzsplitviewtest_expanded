//
//  AppDelegate.h
//  FZSplitViewTest
//
//  Created by Fan Zhang on 6/5/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


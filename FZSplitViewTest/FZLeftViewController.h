//
//  FZLeftViewController.h
//  FZSplitViewTest
//
//  Created by Fan Zhang on 6/5/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DisplayStrUpdateDelegate <NSObject>

- (void)displayStrUpdate:(NSString*)displayStr;

@end

@interface FZLeftViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *displayStrs;
@property (nonatomic, weak) id<DisplayStrUpdateDelegate> delegate;

@end

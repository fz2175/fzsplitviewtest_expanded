//
//  FZLeftViewController.m
//  FZSplitViewTest
//
//  Created by Fan Zhang on 6/5/14.
//  Copyright (c) 2014 Fan Zhang. All rights reserved.
//

#import "FZLeftViewController.h"
#import "FZRightViewController.h"

@interface FZLeftViewController ()

@end

@implementation FZLeftViewController

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:(NSCoder *)aDecoder];
    if (self) {
        // Custom initialization
        self.displayStrs = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.displayStrs.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.displayStrs[indexPath.row];
    
    return cell;
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {

        FZRightViewController *destinationViewController = [segue destinationViewController];
        self.splitViewController.delegate = destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        [(FZRightViewController *)[segue destinationViewController] setDisplayStr:self.displayStrs[indexPath.row]];

    }
}

@end
